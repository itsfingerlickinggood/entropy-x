import React, { useEffect } from "react";
import { WavyBackground } from "../components/wavy-background";
import Link from "next/link";
import "../styles/about.css";

const AboutPage = () => {
  useEffect(() => {
    const link = document.createElement("link");
    link.rel = "icon";
    link.type = "image/jpg";
    link.href = "/images/logo.jpg";
    document.head.appendChild(link);
    return () => {
      document.head.removeChild(link);
    };
  }, []);
  return (
    <div>
      <title>About</title> {/* Set your title here */}
      <Link href="/">
        <span className="space-between nav1 text-2xl">Home</span>
      </Link>
      <WavyBackground className="max-w-4xl mx-auto pb-40">
        <p className="text-2xl md:text-4xl lg:text-7xl text-white font-bold inter-var text-center words">
          About
        </p>
        <p className="text-base md:text-lg mt-4 text-white font-normal inter-var text-center words">
          Pro Futuro. Pro Deo. Pro Populo. Pro Bono Publico - X .
        </p>
      </WavyBackground>
    </div>
  );
};

export default AboutPage;
