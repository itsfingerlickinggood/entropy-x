import React, { useState, useEffect, ChangeEvent, FormEvent } from "react";
import { GoogleGenerativeAI } from "@google/generative-ai";
import { PlaceholdersAndVanishInput } from "../components/placeholders-and-vanish-input";
import "../styles/ideas.css";
import Link from "next/link";

// Access the environment variable
const API_KEY = process.env.NEXT_PUBLIC_API_KEY;

if (!API_KEY) {
  throw new Error(
    "API key is missing. Please set the NEXT_PUBLIC_API_KEY environment variable."
  );
}

const PlaceholdersAndVanishInputDemo = () => {
  const placeholders = [
    "seek and ye shall find",
    "the answer lies within yourself",
  ];

  const [answer, setAnswer] = useState("");
  const [question, setQuestion] = useState("");
  const [showAnswer, setShowAnswer] = useState(false);

  useEffect(() => {
    // Function to fetch the favicon dynamically
    const fetchFavicon = () => {
      // Create a link element
      const link = document.createElement("link");

      // Set the attributes for the link element
      link.rel = "icon";
      link.type = "image/jpg"; // Set the type of favicon (e.g., image/png, image/svg+xml, etc.)
      link.href = "/images/logo.jpg"; // Set the path to your favicon

      // Append the link element to the head of the document
      document.head.appendChild(link);
    };

    // Call the fetchFavicon function to fetch the favicon
    fetchFavicon();
  }, []);

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (question.trim() === "") {
      return;
    }

    const genAI = new GoogleGenerativeAI(API_KEY);
    const model = genAI.getGenerativeModel({ model: "gemini-pro" });

    try {
      const result = await model.generateContent(question);
      const response = await result.response;
      const text = await response.text();
      const formattedText = formatText(text); // Format the generated text
      setAnswer(formattedText);
      setShowAnswer(true);
      console.log("Question:", question);
      console.log("Answer:", formattedText);
    } catch (error) {
      console.error("Error generating text:", error);
    }
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setQuestion(e.target.value);
  };

  // Function to format the generated text as a string
  const formatText = (text: string) => {
    // Split the text into sections based on double newline characters ("\n\n")
    const sections = text.split("\n\n");

    // Organize each section into coherent sentences
    const formattedSections = sections.map((section) => {
      // Check if the section contains bullet points
      const isBulletPoints = section.includes("*");

      // If the section contains bullet points, wrap each bullet point in a list item tag
      if (isBulletPoints) {
        const bulletPoints = section.split("*").map((item, i) => {
          if (i > 0 && item.trim() !== "") {
            return `- ${item.trim()}\n`;
          }
          return null;
        });

        return bulletPoints.join("");
      }

      // If the section does not contain bullet points, return it as a paragraph
      return `${section}\n\n`;
    });

    return formattedSections.join("");
  };

  const handleBack = () => {
    setShowAnswer(false);
    setAnswer("");
  };

  return (
    <div className="relative w-full h-screen flex justify-center items-center bg-white overflow-hidden">
      <title>Ideas</title>
      <Link href="/">
        <span
          className={`absolute top-5 left-5 text-2xl text-white z-50 cursor-pointer back-button2 ${
            showAnswer ? "blur-lg" : ""
          } z-10`}
        >
          Home
        </span>
      </Link>
      <div
        className={`absolute inset-0 flex justify-center items-center transition-all duration-500 ${
          showAnswer ? "blur-lg" : ""
        } z-0`}
      >
        <img
          src="/images/creation.jpg"
          alt="Creation"
          className="block max-w-full max-h-full rounded-full grayscale"
        />
      </div>
      <div
        className={`absolute inset-0 flex justify-center items-center transition-all duration-500 ${
          showAnswer ? "blur-lg" : ""
        } z-10`}
      >
        <div className="flex flex-col justify-center items-center px-4">
          <h2 className="sm:mb-20 text-xl text-center sm:text-5xl text-black font-bold">
            <span className="title-main">"Seek And You Shall Find."</span>
          </h2>
          <PlaceholdersAndVanishInput
            placeholders={placeholders}
            onChange={handleChange}
            onSubmit={handleSubmit}
          />
        </div>
      </div>
      {showAnswer && (
        <div className="absolute inset-0 flex justify-center items-center z-50 animate-fade-in">
          <div className="text-black p-6 rounded-lg max-w-2xl h-80 overflow-y-auto">
            <h2 className="text-2xl font-bold mb-4">
              <span className="heading-answer">Answer</span>
            </h2>
            <p className="leading-loose tracking-wide text-left">{answer}</p>
            <button
              onClick={handleBack}
              className="absolute top-5 left-4 text-black font-bold bg-blue-500 px-4 py-2 rounded"
            >
              Back
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default PlaceholdersAndVanishInputDemo;
