import React, { useEffect, useRef } from "react";
import "../styles/vision.css";
import Link from "next/link";

const Layout = () => {
  const containerRefs = [
    useRef<HTMLDivElement>(null),
    useRef<HTMLDivElement>(null),
    useRef<HTMLDivElement>(null),
  ];

  useEffect(() => {
    // Function to fetch the favicon dynamically
    const fetchFavicon = () => {
      // Create a link element
      const link = document.createElement("link");

      // Set the attributes for the link element
      link.rel = "icon";
      link.type = "image/jpg"; // Set the type of favicon (e.g., image/png, image/svg+xml, etc.)
      link.href = "/images/logo.jpg"; // Set the path to your favicon

      // Append the link element to the head of the document
      document.head.appendChild(link);
    };

    // Call the fetchFavicon function to fetch the favicon
    fetchFavicon();

    // Your existing useEffect logic
    const scrollSpeed = 0.5;

    const updateScrollPos = (
      container: HTMLDivElement,
      scrollPosRef: React.MutableRefObject<number>
    ) => {
      return () => {
        let scrollPos = scrollPosRef.current;
        scrollPos += scrollSpeed;
        container.scrollTop = scrollPos;
        if (scrollPos >= container.scrollHeight / 2) {
          scrollPos = 0;
        }
        scrollPosRef.current = scrollPos;
      };
    };

    const intervals = containerRefs.map((ref) => {
      const container = ref.current;
      const scrollPosRef = { current: 0 };
      if (container) {
        const scrollLoop = setInterval(
          updateScrollPos(container, scrollPosRef),
          1000 / 60
        );
        return scrollLoop;
      }
      return null;
    });

    return () => {
      intervals.forEach((interval) => interval && clearInterval(interval));
    };
  }, []);

  const renderImageGrid = (
    images: string[],
    ref: React.RefObject<HTMLDivElement>
  ) => (
    <div className="scroll-container" ref={ref}>
      <div className="scroll-content">
        {images.concat(images).map((img, index) => (
          <div
            key={index}
            className="h-48 bg-white flex justify-center items-center"
          >
            <div className="w-3/4 h-3/4 flex justify-center items-center">
              <img
                className="max-w-full max-h-full grayscale"
                src={`/images/img/${img}.jpg`}
                alt={`Image ${index + 1}`}
              />
            </div>
          </div>
        ))}
      </div>
    </div>
  );

  return (
    <div className="flex justify-center items-center h-screen bg-white text-black">
      <title>Vision</title>{" "}
      <Link href="/">
        <span className="absolute top-5 left-5 text-2xl text-black z-50 cursor-pointer back-button">
          Home
        </span>
      </Link>
      <div className="grid grid-cols-2 gap-4">
        {renderImageGrid(["a", "b", "c", "d"], containerRefs[0])}
        {renderImageGrid(["e", "f", "g", "h"], containerRefs[1])}
      </div>
      <div className="grid grid-cols-2 gap-4">
        {renderImageGrid(["i", "j", "k", "l"], containerRefs[2])}
        {renderImageGrid(["m", "n", "o", "p"], containerRefs[0])}
      </div>
      <div className="grid grid-cols-2 gap-4">
        {renderImageGrid(["q", "r", "s", "t"], containerRefs[1])}
        {renderImageGrid(["u", "v", "w", "x"], containerRefs[2])}
      </div>
      <div className="absolute inset-0 flex flex-col justify-center items-center text-center z-10">
        <h1 className="text-3xl font-bold mb-4 heading tracking-widest">
          Vision
        </h1>
        <p className="paragraph">
          At Entropy-X, we embrace the chaos of AI, transforming complexity into
          groundbreaking exploration. We fearlessly venture into the unknown,
          breaking conventions to push the boundaries of innovation. At
          Entropy-X, we dare to explore what others won't, driving the future
          forward.
        </p>
      </div>
    </div>
  );
};

export default Layout;
